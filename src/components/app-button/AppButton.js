// import "./app-button.css";

export function AppButton(label) {
    const button = document.createElement("button");
  
    button.innerText = label; // <button>label</button>
  
    return button;
  }
  