import { AppButton } from "../app-button/AppButton";
import "./app-card.css";

/**
 *
 * @param {*} props { showButton: boolean, body: { content: "<p>" } }
 */
export function AppCard(props) {
  const card = document.createElement("article");

  const body = document.createElement("div");
  body.innerHTML = props.body.content;

  const subheading = document.createElement("div");
  subheading.innerHTML = props.subheading.content;

  card.append(body);
  card.append(subheading);

  if (props.showButton) {
    const button = AppButton(props.button.label);

    card.prepend(button);
  }

  return card;
}
