import { AppButton } from "../../../components/app-button/AppButton";
import { AppCard } from "../../../components/app-card/AppCard";
import { loadAssigment } from "../api";

export async function AppWidgetAssigment() {
  const assigments = await loadAssigment();

  console.log(assigments);

  const card = AppCard(assigments[0]);

  const buttons = [AppButton("View all"), AppButton("Grade")];

  const widget = document.createElement("div");

  widget.append(card);
  widget.append(...buttons);

  return widget;
}
