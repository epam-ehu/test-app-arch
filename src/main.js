import "./styles.css";
import { AppButton } from "./components/app-button/AppButton";
import { AppCard } from "./components/app-card/AppCard";
import { AppWidgetAssigment } from "./widgets/assigment/ui/AppWidgetAssigment";

const app = document.querySelector("#app"); // div#app

app.append(await AppWidgetAssigment());
